﻿// Middle2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class MyClass
{
public:
    MyClass( int c,int r,float f, const string s)
    {
        row = r;
        col = c;
         
        float_class = f;
        string_class = s;

        arr = new int* [col];
        for (int i = 0; i < col; i++)
        {
            arr[i] = new int[row];
            for (int j = 0; j < row; j++)
            {
                arr[i][j] = i+j;
            }
        }

    }

    ~MyClass()
    {
        for (int i = 0; i < row; i++)
        {
            delete[] arr[i];
        }
        delete[] arr;
    }

   


    MyClass(const MyClass& other)
    {
         row = other.row;
         col = other.col;
         
         float_class = other.float_class;
         string_class = other.string_class;
         arr = new int*[col];
         for (int i = 0; i < col; i++)
         {
             arr[i] = new int[row];
             for (int j = 0; j < row; j++)
             {
                 arr[i][j] = other.arr[i][j];
             }
         }
    }

    int get_arr_element(int c,int r)
    {
        return arr[c][r];
    }

private:
    int row;
    int col;
     
    int **arr;
    float float_class;
    string string_class;

};

int main()
{
    //std::cout << "Hello World!\n";

    
    MyClass v1(2, 2, 5.6, "bla");
    MyClass v2(v1);
    cout << v2.get_arr_element(0, 0) <<'\n';
    cout << v2.get_arr_element(0, 1) << '\n';
    cout << v2.get_arr_element(1, 0) << '\n';
    cout << v2.get_arr_element(1, 1);
    

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
